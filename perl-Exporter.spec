Name:           perl-Exporter
Version:        5.78
Release:        2
Summary:        Implements default import method for modules
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Exporter
Source0:        https://cpan.metacpan.org/authors/id/T/TO/TODDR/Exporter-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators 
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Carp) >= 1.05 perl(strict) perl(warnings)
BuildRequires:  perl(Test::More) perl(vars)
BuildRequires:  perl(Test::Pod) >= 1.18
Requires:       perl(Carp) >= 1.05 perl(warnings)

%description
The Exporter module implements an import method which allows a module to
export functions and variables to its users' name spaces. Many modules use
Exporter rather than implementing their own import method because Exporter
provides a highly flexible interface, with an implementation optimized for
the common case.

%package            help
Summary:            Documents for %{name}
Buildarch:          noarch

%description        help
Man pages and other related documents for %{name}.

%prep
%autosetup -n Exporter-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 5.78-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jan 08 2025  openeuler_bot <infra@openeuler.sh> - 5.78-1
- Changes for version 5.78
  - Minor tweaks to Makefile.PL to reflect that the file require 5.006,
  - Fixed the LICENSE field to be 'perl_5' and not 'perl'.
  - Whitespace cleanup on Changes

* Tue Jul 18 2023 leeffo <liweiganga@uniontech.com> - 5.77-1
- upgrade to version 5.77

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 5.74-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jul 23 2020 xinghe <xinghe1@huawei.com> - 5.74-1
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: update version to 5.74

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.73-420
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: update the package with tarball

* Sat Sep 14 2019 guiyao <guiyao@huawei.com> - 5.73-419
- Package Init
